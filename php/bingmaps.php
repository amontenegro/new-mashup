<?php
	class Maps extends Thread{
		private $baseURL = 'http://dev.virtualearth.net/REST/v1/Imagery/Map/Road/';
		private $startTime = null;
		private $endTime = null;
		private $res = null;
		private $lat = null;
		private $lng = null;
		private $api_key = null;
		
		public function setLng($lng){
			$this->lng = $lng;
		}
		
		public function setLat($lat){
			$this->lat = $lat;
		}
		
		public function setApiKey($key){
			$this->api_key = $key;
		}
		
		public function getTime(){
			return $endTime - $startTime;
		}
		
		public function getMap(){
			return $this->res;
		}
		
		public function run(){
			$this->startTimeVenue = microtime(true);
				$url = $this->baseURL . $this->lat . "," . $this->lng . "/16?format=jpeg&key=" . $this->api_key;
				echo "<br>". $url . "<br>";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$this->res = curl_exec($ch);
				curl_close($ch);
			$this->endTime = microtime(true);
		}	
	}
?>