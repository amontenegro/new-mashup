<?php
	class Foursquare extends Thread{
		private $baseURL = "https://api.foursquare.com/v2/venues/search?";
		private $oauth = null;
		private $lat = null;
		private $lng = null;
		private $resVenue = null;
		private $startTimeVenue = null;
		private $endTimeVenue = null;
		
		public static function create() {
        	$instance = new self();
        	return $instance;
    	}
		
		public function getTimeVenue(){
			return $this->endTimeVenue - $this->startTimeVenue;
		}
		
		public function setLat($lat){
			$this->lat = $lat;
		}
		
		public function setLng($lng){
			$this->lng = $lng;
		}
		
		public function getLat(){
			return $this->lat;
		}
		
		public function getLng(){
			return $this->lng;
		}
		
		public function setOauthToken($token){
			$this->oauth = $token;
		}
		
		public function getOauthToken(){
			return $this->oauth;
		}
		
		public function getResponse(){
			return $this->resVenue;
		}
		
		public function run(){
			$this->startTimeVenue = microtime(true);
				$ch = curl_init();
				$url = $this->baseURL . "ll=" . $this->getLat() . "," . $this->getLng() . "&oauth_token=" . $this->getOauthToken() . "&v=20140415"; 
				//echo $url . "<br>";
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				/*curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Connection: Keep-Alive',
					'Keep-Alive: 300'
				));*/
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				
				//DEBUG
				/*// CURLOPT_VERBOSE: TRUE to output verbose information. Writes output to STDERR, 
				// or the file specified using CURLOPT_STDERR.
				/curl_setopt($ch, CURLOPT_VERBOSE, true);

				$verbose = fopen('php://temp', 'rw+');
				curl_setopt($ch, CURLOPT_STDERR, $verbose);
				//You can then read if after curl has done the request:

				//$result = curl_exec($curlHandle);
				$json = curl_exec($ch);
				if ($json === FALSE) {
					printf("cUrl error (#%d): %s<br>\n", curl_errno($ch),
						   htmlspecialchars(curl_error($ch)));
				}

				rewind($verbose);
				$verboseLog = stream_get_contents($verbose);

				echo "Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";
				------------------------------------------------------------------------*/
				$json = curl_exec($ch);
				curl_close($ch);
				$this->resVenue = json_decode($json, true);
			$this->endTimeVenue = microtime(true);
			
		}
	}
?>