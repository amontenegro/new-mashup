	<?php
	class LastFmGenericClient extends Thread{
		private $baseURL = "http://ws.audioscrobbler.com/2.0/?";
		private $apiKey = null;
		private $method = null;
	
		//FOR GEO METHODS
		private $long = null;
		private $lat = null;
		private $limitGeo = null;
		private $resGeo = null;
		private $startTimeGeo = null;
		private $endTimeGeo = null;
		
		
		//FOR ARTISTS METHODS
		
		private $listOfSimilarsArtits = null;
		private $limitArtists = null;
		private $artist = null;
		private $startTimeArtist = 0;
		private $endTimeArtist = 0;
		
		
		public function setArtist($artist){
			$this->artist = $artist;
		}
		
		public function setLimitArtists($limit){
			$this->limitArtists = $limit;
		}
		
		public static function create() {
        	$instance = new self();
        	return $instance;
    	}
		
		public function setApiKey($key){
			$this->apiKey = $key;
		}
		
		public function setMethod($method){
			$this->method = $method;
		}
		
		public function setLong($long){
			$this->long = $long;
		}
		
		public function setLat($lat){
			$this->lat = $lat;
		}
		
		public function setLimitGeo($limit){
			$this->limitGeo = $limit;
		}
		
		public function getLat(){
			return $this->lat;
		}
		
		public function getLng(){
			return $this->long;
		}
		
		public function run(){
			if($this->method == "geo.getevents"){				
				$this->startTimeGeo = microtime(true);
					$ch = curl_init();
					$url = $this->baseURL . "method=" .	$this->method . "&limit=" . $this->limitGeo . "&lat=" . $this->lat . "&long=" . $this->long . "&api_key=" . $this->apiKey . "&format=json"; 
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$json = curl_exec($ch);
					curl_close($ch);
					$this->resGeo = json_decode($json,true);
				$this->endTimeGeo = microtime(true);
			}else if($this->method == 'artist.getSimilar'){
				$this->startTimeArtist = microtime(true);
					$ch = curl_init();
					$url = $this->baseURL . "method=" . $this->method . "&limit=" . $this->limitArtists	. "&artist=" . $this->artist . "&api_key=" . $this->apiKey . "&format=json"; 
					//echo $url . "<br>";
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$json = curl_exec($ch);
					curl_close($ch);
					$this->listOfSimilarsArtits = json_decode($json, true);
				$this->endTimeArtist = microtime(true);
			}
		}
		
		public function getResGeo(){
			return $this->resGeo;
		}	
		
		public function getListOfSimilarsArtist(){
			return $this->listOfSimilarsArtits;
		}
		
		public function getTimeArtist(){
			return $this->endTimeArtist - $this->startTimeArtist;
		}
		
		public function getTimeGeo(){
			return $this->endTimeGeo - $this->startTimeGeo;
		}
	}	

	/*$object = LastFmGenericClient::create(); 
	$object->setMethod("geo.getevents");
	$object->setLimit("10");
	//Colocar o ponto.
	$object->setLat("-22.908300");
	$object->setLong("-43.197077");
	$object->setApiKey("6546b7e9a647d8058b75f84431f66c7f");
	$object->call();*/
?>
